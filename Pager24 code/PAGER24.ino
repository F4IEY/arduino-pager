/**
 * PAGER24 Arduino
 * Permet d'envoyer des messages page (style tatoo) sur un PC
 * avec des nRF24l01+   
 */
#include <SPI.h>
#include <RF24.h>
#include <string.h>
#define MYID "0055569" //le numéro d'identification du pager
RF24 tx (7, 8); //CE, CSN
byte adresses[][6] = {"0"}; //fonction de node: sorte de code couleur entre les modules
//on crée un objet package qui contient les données a envoye ou recevoir
struct package {
  char thisId[8]; //l'ID recu ou envoyé en commande
  char myId[8]; //l'id du pager ou l'ID from
  char pageMessage[64]; //le message à envoyer
};
struct package dataPage; //on crée "l'instance"
boolean monitorAll = true; //permet de passer en master/slave

void setup() {
  Serial.begin(115200); //vitesse UART requise pour les données
  SPI.begin();
  //paramètres du TX
  tx.begin();
  tx.setChannel(69); /*le module a 125 canaux (0-124)
  de 2400 à 2525 MHz
  */
  tx.setPALevel(RF24_PA_MAX); //puissance tx au max
  tx.setDataRate(RF24_250KBPS); //vitesse de transmission la plus lente pour plus de portée
  tx.openWritingPipe(adresses[0]); //on precise les slots numériques à ouvrir
  tx.openReadingPipe(1,adresses[0]);
  delay(15);
}

void loop() {
    tx.startListening(); //mode RX
    Serial.print("ID: ");
     while(!Serial.available()) {
      if(tx.available() && tx.isChipConnected()) {         
         tx.read(&dataPage, sizeof(dataPage)); 
         if(strcmp(dataPage.thisId, MYID) == 0) Serial.println("(PRIVATE)");
         else Serial.println("(PUBLIC)");
         if(strcmp(dataPage.thisId, MYID) == 0 || monitorAll) { //on filtre en mode whisper
            //on affiche les données recues
            Serial.print("From: ");
            Serial.println(dataPage.myId);
            Serial.println(dataPage.pageMessage);
          } 
          Serial.println("ID:");
      }
    }
    //mode TX
    tx.stopListening();
    //on entre l'id du correspondant dans le paquet
    if(Serial.available())
       Serial.readString().toCharArray(dataPage.thisId, sizeof(dataPage.thisId));
     Serial.print(dataPage.thisId); 
  //l'ID du Pager
  strcpy(dataPage.myId, MYID);
  //le message:
  Serial.print("\nText (16 characters max): ");
  while(!Serial.available()) {} //on attend la réponse
  //on assigne les 32 premiers caractères du message:
  if(Serial.available()<=64) Serial.readString().toCharArray(dataPage.pageMessage, sizeof(dataPage.pageMessage));
  Serial.print(dataPage.pageMessage);
  Serial.println("\nSending...");
  //envoi des données
  tx.write(&dataPage, sizeof(dataPage));  
  Serial.println("Done!\n");
  delay(500);
}
